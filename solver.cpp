#include <bits/stdc++.h>

using namespace std;


class Lexer {
public:
	Lexer(string str) {
		exp = str;
//		cout << exp << endl;
	}	
	
	pair<vector<string>, string> getLexerResult() {
		pair<vector<string>, string> result = {vector<string>{}, "OK"};
		removeSpaces();
		bool isValid = fillAndCheckForParentheses();
		if(!isValid) {
			result.second = "INTVALID PARENTHESES ";
			return result;
		}
		isValid = hasInvalidCharacters();
		if(!isValid) {
			result.second = "INVALID CHARACTERS ";
			return result;
		}
		exp = removeParenthesesAndAdjustSigns(0, exp.size() - 1, false);
//		cout << "GOT " << endl;
//		cout << exp << endl;
		result.first = getTokens();
		vector<string> v = result.first;
		return result;
	}	
	

private: 
	unordered_map<int, int> parentheses;
	string exp;
	
	void removeSpaces() {
		string res = "";
		int index = 0;
		//this works in O(n) time complexity
		while(index < exp.size()) {
			while(index < exp.size() && exp[index] == ' ') {
				index++;
			}
			res += exp[index];
			index++;
		}
		exp = res;
	}
	
	bool hasInvalidCharacters() {	
		vector<char> operators = {'-', '+'};
		for(int i = 0; i < operators.size(); i++) {
			for(int j = 0; j < operators.size(); j++) {
				string nested = "";
				nested += operators[i];
				nested += operators[j];
				if(exp.find(nested) != string::npos) {
					return false;
				}
			}
		}
		size_t eq = count(exp.begin(), exp.end(), '=');
		if(eq > 1) {
			return false;
		}		
		
		if(exp.find("(+") != string::npos || exp.find("(-") != string::npos) {
			return false;
		}
		
		return true;
	}

	bool fillAndCheckForParentheses() {
		stack<int> indexes;
		bool result = true;
		for(int i = 0; i < exp.size(); i++) {
			if(exp[i] == '(') {
				indexes.push(i);
			} else if(exp[i] == ')') {
				if(!indexes.empty()) {
					int topIndex = indexes.top();
					parentheses[topIndex] = i;
					indexes.pop();
				} else {
					result = false;
					break;
				}			
			}
		}
		return indexes.empty() && result;
	}


	string removeParenthesesAndAdjustSigns(int left, int right, bool flip) {
		int index = left;
		string res = "";
		while(index <= right) {
			if(exp[index] == '(') {
				bool toFlip = false;
				if(index - 1 >= 0 && exp[index - 1] == '-') {
					toFlip = true;
				}
				res += removeParenthesesAndAdjustSigns(index + 1, parentheses[index] - 1, toFlip);
				index = parentheses[index] + 1;
				continue;
			}
			res += exp[index];
			index++;
		}
		//change the signs if we have a '-' operator before a parenthesis
		if(flip) {
			for(int i = 0; i < res.size(); i++) {
				if(res[i] == '-') res[i] = '+';
				else if(res[i] == '+') res[i] = '-';
			}
		}
		return res;
	}
	
	
	vector<string> getTokens() {
		string cur = "";
		vector<string> res;
		for(int i = 0; i < exp.size(); i++) {
			if((exp[i] == '+' || exp[i] == '-' || exp[i] == '=') && !cur.empty()) {
				res.push_back(cur);
				res.push_back(string(1, exp[i]));
				cur = "";
				continue;
			} else {
				cur += exp[i];
			}
		}	
		if(!cur.empty()) {
			res.push_back(cur);
		}
		return res;
	}	
};

class Parser {
public:
	Parser(const vector<string>& _tokens) {
		tokens = _tokens;
	}

	pair<bool, string> checkTokens() {
		pair<bool, string> result = {true, "OK"};
		for(const auto& token : tokens) {
			if(!isValid(token)) {
				result.first = false;
				result.second = "Invalid token " + token + " ";
				break;
			}
		}
		return result;
	}
	
	pair<vector<double>, string> simplifyExpression(map<string, int>& variables, unordered_set<string>& uniqueVars, bool toFill) {
		fillOperands();
		if(operands.size() >= variables.size()) {
			variables = operands;
		}
		
		vector<double> res;
		string result = "";
		for(const auto& cur : operands) {
			if(!cur.first.empty()) {
				string operand = to_string(cur.second) + cur.first;
				uniqueVars.insert(cur.first);
				if(!result.empty() && !operand.empty() && operand[0] != '-') {
					result += '+';
				}
				result += operand;
			}
		}	
		result += "=" + to_string(-1 * operands[""]);

		if(toFill) {
			for(auto it = variables.begin(); it != variables.end(); it++) {
				if(!(it -> first).empty()) {
					auto myOper = operands.find(it -> first);
					if(myOper != operands.end()) {
						res.push_back((double)myOper -> second);
					} else {
						res.push_back(0);
					}
				}
			}
		}
		res.push_back(-1.0 * operands[""]);
		return {res, result};
	}
	
	
private:
	vector<string> tokens;
	map<string, int> operands; 
	
	void fillOperands() {
		bool afterEqualOperator = false;
		for(int i = 0; i < tokens.size(); i++) {
			string token = tokens[i];
			if(token.size() == 1 && token[0] == '=') afterEqualOperator = true;
			if(!(token.size() == 1 && (token[0] == '-' || token[0] == '+' || token[0] == '='))) { 
				auto cur = getVariable(tokens[i]);
				if(i - 1 >= 0 && tokens[i - 1].size() == 1 && tokens[i - 1][0] == '-') {
					cur.second *= -1;
				}
				if(afterEqualOperator) {
					operands[cur.first] += -1 * cur.second;
				} else {
					operands[cur.first] += cur.second;
				}
			}
		}
	}
	
	
	pair<string, int>  assignValues(int index, const string& str, bool alphaFirst) {
		pair<string, int> res = {"", 1};
		int i = index;
		string cur = "";
		if(alphaFirst) {
			for(; i < str.size(); i++) {
				if(isdigit(str[i])) break;
				cur += str[i];		
			}
			res.first = cur;
			if(!str.substr(i).empty()) {
				res.second = stoi(str.substr(i));
			}
		} else {
			for(; i < str.size(); i++) {
				if(isalpha(str[i])) break;
				cur += str[i];
			}
			res.second = stoi(cur);
			res.first = str.substr(i);
		}
		return res;
	}
	
	pair<string, int> getVariable(const string& str) {
		pair<string, int> res = {"", 1};
		if(str.empty()) {
			return res;
		}
		
		if(str[0] == '+' || str[0] == '-') {
			bool toMult = (str[0] == '-');	
			if(str.size() >= 1 && isalpha(str[1])) {
				res = assignValues(1, str, true);
			} else if(str.size() >= 1 && isdigit(str[1])) {
				res = assignValues(1, str, false);
			}
			if(toMult) {
				res.second *= -1;
			}
		} else if(isalpha(str[0])) {
			res = assignValues(0, str, true);
		} else if(isdigit(str[0])) {
			res = assignValues(0, str, false);
		} 
		return res;
	}
	
	
	bool isValid(string str) {
		if(str.empty()) {
			return false;
		}
		if(str.size() == 1 && (str[0] == '-' || str[0] == '+')) {
			return true;
		}
		bool flag = false;
		for(const auto& letter : str) {
			if(isalpha(letter)) {
				flag = true;
			}
		}
		
		if(flag && isdigit(str[0]) && isdigit(str[str.size() - 1])) {
			return false;
		}
		
		return true;
	}			
};


vector<double> gauss(vector< vector<double> > A) {
    int n = A.size();

    for (int i=0; i<n; i++) {
        // Search for maximum in this column
        double maxEl = abs(A[i][i]);
        int maxRow = i;
        for (int k=i+1; k<n; k++) {
            if (abs(A[k][i]) > maxEl) {
                maxEl = abs(A[k][i]);
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (int k=i; k<n+1;k++) {
            double tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }

        // Make all rows below this one 0 in current column
        for (int k=i+1; k<n; k++) {
            double c = -A[k][i]/A[i][i];
            for (int j=i; j<n+1; j++) {
                if (i==j) {
                    A[k][j] = 0;
                } else {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    vector<double> x(n);
    for (int i=n-1; i>=0; i--) {
        x[i] = A[i][n]/A[i][i];
        for (int k=i-1;k>=0; k--) {
            A[k][n] -= A[k][i] * x[i];
        }
    }
    return x;
}



pair<map<string, int>, bool> analyzeTheSystem(const vector<string>& expressions) {
	map<string, int> variables;
	unordered_set<string> uniqueVars;
	vector<string> simplifiedForm;
	for(int i = 0; i < expressions.size(); i++) {
		Lexer lexer(expressions[i]);
		auto lexerCheck = lexer.getLexerResult();
		if(lexerCheck.second != "OK") {
			cout << lexerCheck.second << " in equation # " << (i + 1) << endl;
			return {variables, false};
		}
		
		Parser parser(lexerCheck.first);
		auto parserCheck = parser.checkTokens();
		if(parserCheck.second != "OK") {
			cout << parserCheck.second << " in equation # " <<  (i + 1) << endl;
			return {variables, false};
		}
		
		pair<vector<double>, string> exp = parser.simplifyExpression(variables, uniqueVars, false);
		simplifiedForm.push_back(exp.second);
	}
	
	if(uniqueVars.size() != expressions.size()) {
		cout << "The system has infinte number of solutions or does not have a solution" << endl;
		cout << "Possible reasons: " << endl;
		cout << "1) The number of unknowns is more than the number of equations!" << endl;
		cout << "2) The system' |detA| = 0, incompatible rank" << endl;
		return {variables, false};
	}
	
	cout << "Simplified form of the equations is: " << endl;
	for(const auto& exp : simplifiedForm) {
		cout << exp << endl;
	}
	cout << endl;
	return {variables, true};
}



void solveLinear(const vector<string>& expressions) {
	auto checkTheSystem = analyzeTheSystem(expressions);
	unordered_set<string> uniqueVars;
	if(!checkTheSystem.second) {
		return;
	}
	vector<vector<double> > matrix;
	
	for(int i = 0; i < expressions.size(); i++) {
		string str = expressions[i];
		Lexer lex(str);
		auto result = lex.getLexerResult();
		if(result.second != "OK") {
			cout << result.second << endl;
			return;
		}
	
		Parser parse(result.first);
		auto exp = parse.simplifyExpression(checkTheSystem.first, uniqueVars, true);
		matrix.push_back(exp.first);
	}
	
	cout << "THE GAUSSIAN MATRIX IS: " << endl;
	for(int i = 0; i < matrix.size(); i++) {
		for(int j = 0; j < matrix[i].size(); j++) {
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	
	int index = 0;	
	auto allVariables = checkTheSystem.first;
	
	vector<double> result = gauss(matrix);
	cout.precision(8);
	cout << "RESULT " << endl;
	for(const auto& var : allVariables) {
		if(!var.first.empty()) {
			cout << var.first << " = " << result[index] << endl;
			index++;		
		}
	}
}





int main() {	
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);	
	int n; string str;
	vector<string> expressions;
	cin >> n;
	cin.ignore();
	for(int i = 0; i < n; i++) {
		getline(cin, str);
		expressions.push_back(str);
	}
	
	solveLinear(expressions);	
	
	
		
	

	return 0;
}